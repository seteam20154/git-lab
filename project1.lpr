program LabWork2var4;

const
	DimensionValue = 9;
	MaxMatrixInitBorder = 23;
	MinMatrixInitBorder = -21;
	InitBordersAbsSumm = MaxMatrixInitBorder - MinMatrixInitBorder + 1;

type
	Matrix = array [1..DimensionValue, 1..DimensionValue] of Integer;

procedure InitMatrix(var MatrixToInit: Matrix);
var
	i,j: Integer;
begin
	for i:=1 to DimensionValue do
	begin
		for j:=1 to DimensionValue do
		begin
		MatrixToInit[i,j] := Random(InitBordersAbsSumm)+MinMatrixInitBorder;;
		end;
	end;
end;

procedure ShowMatrix(var MatrixToShow: Matrix);
var
	i,j: Integer;
begin
	for i:=1 to DimensionValue do
	begin
		for j:=1 to DimensionValue do
		begin
			Write(MatrixToShow[i,j]:4);
		end;
		Writeln;
	end
end;

procedure SecondaryDiagonalPlusElements(var MatrixToWork: Matrix);
var
	i,j, ElementsFound: Integer;

begin
	WriteLn('Searching for positive elements on Secondary Diagonal...');
	ElementsFound:=0;
	for i:=1 to DimensionValue do
	begin
		for j:=1 to DimensionValue do
		begin
			if (i+j < DimensionValue + 1) then
			begin
				ElementsFound := ElementsFound + 1;
			end;
		end;
	end;
	WriteLn;
	WriteLn('Total amount of positive elements on Secondary Diagonal:');
	WriteLn(ElementsFound);
end;

procedure NegativeElementsUnderMainDiagonalSwapper(var MatrixToWork: Matrix);
var
	i,j: integer;
begin
	for i:=1 to DimensionValue do
	begin
		for j:=1 to DimensionValue do
		begin
			if ((i>j) and (MatrixToWork[i,j]<0)) then
			begin
				MatrixtoWork[i,j] := 0;
			end;
		end;
	end;
	WriteLn('Swapper on work - matrix was changed!')
end;

var
	BaseMatrix: Matrix;

begin
	Randomize;
	InitMatrix(BaseMatrix);
	WriteLn('Base matrix has been created:');
	ShowMatrix(BaseMatrix);
	SecondaryDiagonalPlusElements(BaseMatrix);
	NegativeElementsUnderMainDiagonalSwapper(BaseMatrix);
	ShowMatrix(BaseMatrix);
	Readln;
end.

